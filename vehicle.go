package states

import (
	"context"
	"log"
	"time"
)

const (
	TotalBatteryCapacity int64 = 1 * 12 * 60 * 50 // 50 hours (per each tick in 5 sec)
)

var clockUtils = NewClockUtils(defaultClockSource{})

type Vehicle struct {
	StateMachine
	id                string
	state             State
	batteryCapacity   int64
	latestStateChange time.Time
	validators        []VehicleValidator
	stateHandlers     []VehicleStateHandler
	tickHandlers      []TicksHandler
	clock             ClockUtils
}

func (v Vehicle) BatteryCapacity() int64 {
	return v.batteryCapacity
}

func (v Vehicle) State() State {
	return v.state
}

type VehicleStateHandler interface {
	OnStateChanged(v *Vehicle, stateFrom, stateTo State, role *UserRole) error
}

type VehicleOptionFn func(v *Vehicle)

func NewVehicle(id string, opts ...VehicleOptionFn) *Vehicle {
	instance := Vehicle{
		id:                id,
		batteryCapacity:   TotalBatteryCapacity,
		state:             StateReady,
		validators:        make([]VehicleValidator, 0),
		stateHandlers:     make([]VehicleStateHandler, 0),
		tickHandlers:      make([]TicksHandler, 0),
		latestStateChange: clockUtils.Now(),
		clock:             clockUtils,
	}

	for _, opt := range opts {
		opt(&instance)
	}

	return &instance
}

func (v *Vehicle) SwitchState(stateTo State, role *UserRole) error {
	if (role != nil && role.IsAllowedChange(v.state, stateTo)) || role == nil {
		prevState := v.state
		v.latestStateChange = time.Now()
		v.state = stateTo
		if v.stateHandlers != nil {
			for _, stateHandler := range v.stateHandlers {
				if err := stateHandler.OnStateChanged(v, prevState, stateTo, role); err != nil {
					return err
				}
			}
		}
		return nil
	}
	return StatePermissionDenied
}

func (v *Vehicle) OnTick(ctx context.Context) error {
	defer func() {
		for _, handler := range v.tickHandlers {
			if err := handler.OnTick(ctx); err != nil {
				log.Println(err.Error())
			}
		}
	}()

	for _, validator := range v.validators {
		if err := validator.Validate(ctx, v); err != nil {
			return err
		}
	}

	return nil
}
