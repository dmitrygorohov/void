package states_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"sync/atomic"
	"testing"
	"time"
	"voi"
)

type TestTickListener struct {
	counts uint32
}

func (t *TestTickListener) OnTick(ctx context.Context) error {
	atomic.AddUint32(&t.counts, 1)
	return nil
}

func TestNewTickMachine(t *testing.T) {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*11)
	machine := states.NewTickMachine(states.WithTickInterval(2))
	listener := TestTickListener{}
	machine.RegisterListener(&listener)
	ticker, err := machine.Tick(ctx)
	if err != nil {
		panic(err)
	}
	defer func() {
		ticker.Stop()
	}()

	<-ctx.Done()

	assert.EqualError(t, ctx.Err(), "context deadline exceeded")
	assert.Equal(t, uint32(5), listener.counts)
}
