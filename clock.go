package states

import "time"

type Clock interface {
	Now() time.Time
}

type ClockUtils struct {
	source Clock
}

func NewClockUtils(source Clock) ClockUtils {
	return ClockUtils{
		source: source,
	}
}

type defaultClockSource struct{}

func (c defaultClockSource) Now() time.Time {
	return time.Now()
}

func (c ClockUtils) Now() time.Time {
	return c.source.Now()
}

func (c ClockUtils) IsTimeAfter(hour, minute int) bool {
	now := c.source.Now()
	serviceTime := time.Date(now.Year(), now.Month(), now.Day(), hour, minute, 0, 0, now.Location())
	return now.After(serviceTime)
}
