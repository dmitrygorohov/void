package states

import (
	"context"
	"sync/atomic"
	"time"
)

type VehicleValidator interface {
	Validate(ctx context.Context, v *Vehicle) error
}

// Morning validator
type morningValidator struct {
	hours, minutes int
}

func (m morningValidator) Validate(ctx context.Context, v *Vehicle) error {
	now := clockUtils.Now()
	if v.state.Is(GeneralGroup) {
		serviceTime := time.Date(now.Year(), now.Month(), now.Day(), m.hours, m.minutes, 0, 0, now.Location())
		triggerTime := serviceTime.AddDate(0, 0, -1)
		if triggerTime.After(v.latestStateChange) && now.After(serviceTime) {
			return v.SwitchState(StateBounty, nil)
		}
	}
	return nil
}

func WithMorningValidator() VehicleOptionFn {
	return func(v *Vehicle) {
		v.validators = append(v.validators, morningValidator{hours: 9, minutes: 30})
	}
}

// Checking if vehicle is idling
type idleValidator struct {
	idleDurationThreshold time.Duration
}

func (vl idleValidator) Validate(ctx context.Context, v *Vehicle) error {
	if !v.state.Is(ServiceModeGroup) {
		idlingDuration := time.Since(v.latestStateChange)
		if idlingDuration > vl.idleDurationThreshold {
			return v.SwitchState(StateUnknown, nil)
		}
	}
	return nil
}

func WithIdleTrigger(idleThreshold time.Duration) VehicleOptionFn {
	return func(v *Vehicle) {
		v.validators = append(v.validators, idleValidator{idleDurationThreshold: idleThreshold})
	}
}

// Battery check flow
type batteryValidator struct {
	threshold uint8
}

func (vl batteryValidator) Validate(ctx context.Context, v *Vehicle) error {
	if !v.state.Is(BountyGroup, ServiceModeGroup) {
		level := vl.getBatteryLevel(v.batteryCapacity)
		switch {
		case level < 20:
			return v.SwitchState(StateBatteryLow, nil)
		default:
			return nil
		}
	}
	if currentBatteryCapacity := atomic.AddInt64(&v.batteryCapacity, -1); currentBatteryCapacity < 1 {
		return BatteryTooLowError
	}
	return nil
}

func (vl batteryValidator) getBatteryLevel(capacity int64) int {
	return int(100 * capacity / TotalBatteryCapacity)
}

func (vl batteryValidator) OnStateChanged(v *Vehicle, stateFrom, stateTo State, role *UserRole) error {
	if stateFrom.In(StateBatteryLow) {
		return v.SwitchState(StateBounty, role)
	}
	return nil
}

func WithBatteryValidator(percentThreshold uint8) VehicleOptionFn {
	return func(v *Vehicle) {
		v.validators = append(v.validators, batteryValidator{threshold: percentThreshold})
		v.stateHandlers = append(v.stateHandlers, )
	}
}
