package states

import (
	"context"
	"log"
	"time"
)

// --- Ticks
type TicksHandler interface {
	OnTick(ctx context.Context) error
}

type TickMachine struct {
	pingInterval time.Duration
	handlers     []TicksHandler
}

func WithTickInterval(intervalInSeconds int64) TickMachineOptionsFn {
	return func(e *TickMachine) {
		e.pingInterval = time.Duration(intervalInSeconds) * time.Second
	}
}

type TickMachineOptionsFn func(e *TickMachine)

func NewTickMachine(opts ...TickMachineOptionsFn) *TickMachine {
	instance := TickMachine{
		pingInterval: time.Second * 5,
		handlers:     make([]TicksHandler, 0),
	}

	for _, opt := range opts {
		opt(&instance)
	}

	return &instance
}

func (e *TickMachine) RegisterListener(v TicksHandler) {
	e.handlers = append(e.handlers, v)
}

func (e *TickMachine) Tick(ctx context.Context) (*time.Ticker, error) {
	ticker := time.NewTicker(e.pingInterval)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-ticker.C:
				if err := e.callHandlers(ctx); err != nil {
					log.Println(err.Error())
					return
				}
			}
		}
	}()
	return ticker, nil
}

func (e TickMachine) callHandlers(ctx context.Context) error {
	for _, handler := range e.handlers {
		if err := handler.OnTick(ctx); err != nil {
			return err
		}
	}
	return nil
}
