package states

type VehicleError struct {
	message string
}

func (e VehicleError) Error() string {
	return e.message
}

func NewVehicleError(message string) error {
	return VehicleError{message: message}
}

var (
	BatteryTooLowError = VehicleError{message: "battery capacity is too low."}
)

func IsVehicleError(e error) (VehicleError, bool) {
	ve, ok := e.(VehicleError)
	return ve, ok
}
