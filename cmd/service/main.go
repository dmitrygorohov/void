package main

import (
	"context"
	"expvar"
	_ "expvar"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var registry *VehicleRegistry

func main() {
	ctx := context.Background()

	registry = NewRegistry()
	ticker, err := registry.Listen(ctx)
	if err != nil {
		log.Panic(err)
	}
	defer func() {
		ticker.Stop()
	}()

	expvar.Publish("allStats", expvar.Func(registry.Stats))

	r := mux.NewRouter()
	r.HandleFunc("/", indexHandler)
	r.HandleFunc("/add", addVehicleHandler)
	r.Path("/terminate/{id}").HandlerFunc(terminateHandler)
	r.Path("/state/{id}/{state}").HandlerFunc(stateSwitchHandler)
	r.Handle("/debug/vars", http.DefaultServeMux)

	if err := http.ListenAndServe(":9000", r); err != nil {
		log.Panic(err)
	}
}
