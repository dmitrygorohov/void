package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"time"
	. "voi"
)

type VehicleState struct {
	Capacity int64
	Percent  int
	State    State
}

type VehicleRegistry struct {
	ticker      *time.Ticker
	vehicles    map[string]*Vehicle
	tickMachine *TickMachine
}

func NewRegistry() *VehicleRegistry {
	return &VehicleRegistry{
		vehicles: make(map[string]*Vehicle),
		tickMachine: NewTickMachine(
			WithTickInterval(5)),
	}
}

func (v *VehicleRegistry) Listen(ctx context.Context) (*time.Ticker, error) {
	return v.tickMachine.Tick(ctx)
}

func (v *VehicleRegistry) NewVehicle() {
	vehicleId := uuid.New().String()

	vehicle := NewVehicle(vehicleId,
		WithIdleTrigger(48*time.Hour),
		WithMorningValidator(),
		WithBatteryValidator(20),
		WithMetricSupport())
	v.tickMachine.RegisterListener(vehicle)

	v.vehicles[vehicleId] = vehicle
}

func (v *VehicleRegistry) RemoveVehicle(id string, role *UserRole) error {
	if vehicle, ok := v.vehicles[id]; ok {
		delete(v.vehicles, id)
		return vehicle.SwitchState(StateTerminated, role)
	}
	return NewVehicleError(fmt.Sprintf("vehicle with id: %s not found", id))
}

func (v *VehicleRegistry) SwitchState(id string, state int, role *UserRole) error {
	if vehicle, ok := v.vehicles[id]; ok {
		return vehicle.SwitchState(State(state), role)
	}
	return NewVehicleError(fmt.Sprintf("vehicle with id: %s not found", id))
}

func (v VehicleRegistry) Stats() interface{} {
	stats := make(map[string]VehicleState)
	for id, vehicle := range v.vehicles {
		stats[id] = VehicleState{
			Capacity: vehicle.BatteryCapacity(),
			Percent:  int(100 * vehicle.BatteryCapacity() / TotalBatteryCapacity),
			State:    vehicle.State(),
		}
	}
	return stats
}
