package main

import (
	"bytes"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	states "voi"
)

func getUserRole(role string) *states.UserRole {
	switch {
	case len(role) == 0:
		return nil
	case role == "admin":
		return &states.Admin
	case role == "user":
		return &states.User
	case role == "hunter":
		return &states.Hunter
	default:
		return nil
	}
}

func addVehicleHandler(w http.ResponseWriter, request *http.Request) {
	registry.NewVehicle()
	writeContent(w, `<html><body>
Added! <br />You can go to <a href="/debug/vars">stats</a> page to see the progress.
</body></html>
`)
}

func stateSwitchHandler(w http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	role := request.FormValue("role")
	state, err := strconv.Atoi(vars["state"])
	message := "Switched!"
	if err != nil {
		log.Println(err.Error())
		message = err.Error()

	} else if err := registry.SwitchState(vars["id"], state, getUserRole(role)); err != nil {
		log.Println(err.Error())
		message = err.Error()
	}

	writeContent(w, `<html><body>`+message+`<br />You can go to <a href="stats">stats</a> page to see the progress.
</body></html>
`)
}

func terminateHandler(w http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	role := request.FormValue("role")
	message := "Terminated!"
	if err := registry.RemoveVehicle(vars["id"], getUserRole(role)); err != nil {
		log.Println(err.Error())
		message = err.Error()
	}

	writeContent(w, `<html><body>`+message+`<br />You can go to <a href="stats">stats</a> page to see the progress.
</body></html>
`)

}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	buf := bytes.Buffer{}
	buf.WriteString("¡Hola Amigo!")

	w.WriteHeader(200)
	if _, err := w.Write(buf.Bytes()); err != nil {
		log.Println(err.Error())
	}
}

func writeContent(w http.ResponseWriter, content string) {
	buf := bytes.Buffer{}
	buf.WriteString(content)
	w.WriteHeader(200)
	w.Header().Add("Content-Type", "text/html")
	if _, err := w.Write(buf.Bytes()); err != nil {
		log.Println(err.Error())
	}
}
