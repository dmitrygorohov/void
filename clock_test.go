package states

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

type testClockSource struct {
	nowTime time.Time
}

func (t testClockSource) Now() time.Time {
	return t.nowTime
}

func TestClock_Now(t *testing.T) {
	clock := defaultClockSource{}
	secDiff := int(time.Since(clock.Now()).Seconds())
	assert.Equal(t, 0, secDiff)
}

func TestClock_IsTimeAfter(t *testing.T) {
	now := time.Now()

	t.Run("case", func(t *testing.T) {
		utils := NewClockUtils(testClockSource{
			nowTime: time.Date(now.Year(), now.Month(), now.Day(), 8, 30, now.Second(), now.Nanosecond(), now.Location()),
		})
		assert.False(t, utils.IsTimeAfter(9, 30))
	})

	t.Run("case", func(t *testing.T) {
		utils := NewClockUtils(testClockSource{
			nowTime: time.Date(now.Year(), now.Month(), now.Day(), 10, 30, now.Second(), now.Nanosecond(), now.Location()).
				AddDate(0, 0, -1),
		})
		assert.True(t, utils.IsTimeAfter(9, 30))
	})

	t.Run("case", func(t *testing.T) {
		utils := NewClockUtils(testClockSource{
			nowTime: time.Date(now.Year(), now.Month(), now.Day(), 10, 30, now.Second(), now.Nanosecond(), now.Location()).
				AddDate(0, 0, 1),
		})
		assert.True(t, utils.IsTimeAfter(9, 30))
	})

	t.Run("case", func(t *testing.T) {
		nowTime := time.Date(now.Year(), now.Month(), now.Day(), 10, 30, now.Second(), now.Nanosecond(), now.Location()).
			AddDate(0, 0, 1)
		utils := NewClockUtils(testClockSource{
			nowTime: nowTime,
		})
		assert.Equal(t, nowTime, utils.Now())
	})
}
