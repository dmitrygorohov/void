package states

import (
	"context"
	"expvar"
)

// Metrics support, available through /debug/vars, published via expvar
type VehicleMetric struct {
	capacityMetric *expvar.Int
	stateMetric    *expvar.Int
	v              *Vehicle
}

func (v *VehicleMetric) OnTick(ctx context.Context) error {
	v.capacityMetric.Add(-1)
	v.stateMetric.Set(int64(v.v.state))
	return nil
}

func WithMetricSupport() VehicleOptionFn {
	return func(v *Vehicle) {
		capacityMetric := expvar.NewInt("battery_" + v.id)
		capacityMetric.Set(TotalBatteryCapacity)

		stateMetric := expvar.NewInt("state_" + v.id)
		stateMetric.Set(int64(v.state))

		v.tickHandlers = append(v.tickHandlers, &VehicleMetric{
			capacityMetric: capacityMetric,
			stateMetric:    stateMetric,
			v:              v,
		})
	}
}
