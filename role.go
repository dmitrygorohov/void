package states

import "errors"

var (
	StatePermissionDenied = errors.New("not allowed to change a state")
)

var (
	User = UserRole{allowedTransitions: map[State]States{
		StateReady: {StateRiding},
	}}
	Hunter = UserRole{allowedTransitions: map[State]States{
		StateBounty:    {StateCollected},
		StateCollected: {StateDropped},
		StateDropped:   {StateReady},
	}}
	Admin = UserRole{}
)

type UserRole struct {
	allowedTransitions map[State]States
}

func (u UserRole) IsAllowedChange(currentState, state State) bool {
	if u.allowedTransitions != nil {
		if v, ok := u.allowedTransitions[currentState]; ok {
			return v.In(state)
		}
		return false
	}
	return true
}
