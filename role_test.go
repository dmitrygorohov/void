package states_test

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"voi"
)

func TestUserStateChangeNotPermitted(t *testing.T) {
	scooter := states.NewVehicle("1")
	err := scooter.SwitchState(states.StateCollected, &states.User)
	assert.Error(t, err)
}

func TestAdminStateChangeNotPermitted(t *testing.T) {
	scooter := states.NewVehicle("1")
	err := scooter.SwitchState(states.StateCollected, &states.Admin)
	assert.NoError(t, err)
}

func TestHunterStateChangeNotPermitted(t *testing.T) {
	scooter := states.NewVehicle("1")
	err := scooter.SwitchState(states.StateBounty, &states.Hunter)
	assert.Error(t, err)
}
