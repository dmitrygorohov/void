package states

type State int

const (
	GeneralGroup     int = 0x01
	BountyGroup      int = 0x04
	ServiceModeGroup int = 0x10
)

const (
	StateReady       = State(GeneralGroup)
	StateRiding      = State(int(StateReady) + 1 | GeneralGroup)
	StateBatteryLow  = State(int(StateRiding) + 1 | GeneralGroup)

	StateBounty      = State(BountyGroup)
	StateCollected   = State(int(StateBounty) + 1 | BountyGroup)
	StateDropped     = State(int(StateCollected) + 1 | BountyGroup)

	StateUnknown     = State(ServiceModeGroup)
	StateServiceMode = State(int(StateUnknown) + 1 | ServiceModeGroup)
	StateTerminated  = State(int(StateServiceMode) + 1 | ServiceModeGroup)
)

func (s State) Is(groups ... int) bool {
	for _, group := range groups {
		if int(s)&group == group {
			return true
		}
	}
	return false
}

type States []State

func (s States) In(state State) bool {
	for _, inState := range s {
		if inState == state {
			return true
		}
	}
	return false
}

func (s State) In(states ...State) bool {
	for _, state := range states {
		if s == state {
			return true
		}
	}
	return false
}

func (s State) NotIn(states ...State) bool {
	return !s.In(states ...)
}

type StateMachine interface {
	SwitchState(state State, role UserRole) error
}
