module voi

require (
	github.com/google/uuid v1.1.1
	github.com/gorilla/mux v1.7.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/stretchr/testify v1.3.0
)
