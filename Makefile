all: test build

build:
	@docker-compose up --build -d --remove-orphans

.PHONY: test
test:
	@GO111MODULE=on go test -race ./...

stop:
	@docker-compose stop