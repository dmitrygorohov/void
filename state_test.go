package states_test

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"voi"
)

func TestGroups(t *testing.T) {
	items := []struct {
		state  states.State
		group  int
		result bool
	}{
		{
			state:  states.StateReady,
			group:  states.GeneralGroup,
			result: true,
		},
		{
			state:  states.StateRiding,
			group:  states.GeneralGroup,
			result: true,
		},
		{
			state:  states.StateBatteryLow,
			group:  states.GeneralGroup,
			result: true,
		},
		{
			state:  states.StateRiding,
			group:  states.BountyGroup,
			result: false,
		},
		{
			state:  states.StateBounty,
			group:  states.BountyGroup,
			result: true,
		},
		{
			state:  states.StateCollected,
			group:  states.BountyGroup,
			result: true,
		},
		{
			state:  states.StateDropped,
			group:  states.BountyGroup,
			result: true,
		},
		{
			state:  states.StateUnknown,
			group:  states.BountyGroup,
			result: false,
		},
		{
			state:  states.StateUnknown,
			group:  states.ServiceModeGroup,
			result: true,
		},
		{
			state:  states.StateUnknown,
			group:  states.ServiceModeGroup,
			result: true,
		},
		{
			state:  states.StateTerminated,
			group:  states.ServiceModeGroup,
			result: true,
		},
	}
	for _, item := range items {
		assert.Equal(t, item.result, item.state.Is(item.group), "failed with %d %d %v", item.state, item.group, item.result)
	}
}
