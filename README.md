To build: `make `

By default hosting on `:9000` port.

API:

- To add a new vehicle:
    `/add`
- To terminate an instance:
    `/terminate/{id}?role={role}`
    
    where's `role = [admin,user,hunter]` or empty/nil
    
    Example: http://localhost:9000/terminate/c6b56e78-73eb-43e3-9df0-ec4d8e3595e0?role=admin 

- To switch an instance's state:
    `/state/{id}/{state}?role={role}`
    
    where's `role = [admin,user,hunter]` or empty/nil
    
    Example: http://localhost:9000/state/c6b56e78-73eb-43e3-9df0-ec4d8e3595e0/10?role=user
    
- To dump all register vehicle's stats:
    `/debug/vars`

- To stop it: `make stop`